import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


public class FirstGUI extends JFrame {
	private JTextField name;
	public FirstGUI(){
		super("Hello");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(357, 162);
		JLabel myLabel = new JLabel("I'm sorry, this form has only one button. See below.");
		name = new JTextField(20);
		this.add(myLabel, BorderLayout.NORTH);
		JButton button = new JButton("How are you?");
		add(button,BorderLayout.SOUTH);
		button.addActionListener(new ButtonListenter());
	}

	private class ButtonListenter implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			String message;
			String response = JOptionPane.showInputDialog("Are you feeling good today?");
			if(response.equalsIgnoreCase("yes")){
				message = "It’s great to be alive!";
			}else message = "So you had a bad day…";
			// message += FirstGUI.this.getWidth() + ","+ FirstGUI.this.getHeight();
			JOptionPane.showMessageDialog(null, message);
		}
		
	}
	public static void main(String[] args){
		FirstGUI gui = new FirstGUI();
		gui.setVisible(true);
	}
}
